# Floodboat
# =========
#
# vectors.py

import math

class Vector:
    """A two-dimentional vector"""
    def __init__(self, x=None, y=None, magnitude=None, direction=None):
        if x is not None and y is not None:
            self.x, self.y = x, y
            self.rec_to_pol()

        elif magnitude is not None and direction is not None:
            self.magnitude, self.direction = magnitude, direction
            self.pol_to_rec()
        else:
            raise ValueError('Too many None inputs')

    def rotate(self, angle):
        """Return a vector rotated by the angle"""
        return Vector(magnitude=self.magnitude, direction=self.direction + angle)

    def pol_to_rec(self):
        """Convert this vector from polar to rectangular coordinates"""
        direction = self.direction * math.pi / 180
        self.x = self.magnitude * math.cos(direction)
        self.y = self.magnitude * math.sin(direction)

    def rec_to_pol(self):
        """Convert this vector from rectangular to polar coordinates"""
        self.magnitude = (self.x ** 2 + self.y ** 2) ** 0.5
        self.direction = math.degrees(math.atan2(self.y, self.x))

    def remove_direction(self, direction):
        """Return a vector that is equal to this, but with a direction
           removed completely"""
        newvec = self.rotate(-direction)
        newvec.x = 0
        newvec.rec_to_pol()
        return newvec.rotate(direction)
    
    def to_tuple(self):
        """Return a tuple (x, y) of this vector"""
        return (self.x, self.y)
    
    def __add__(self, other):
        return Vector(x=self.x + other.x, y=self.y + other.y)

    def __sub__(self, other):
        return Vector(x=self.x - other.x, y=self.y - other.y)

    def __mul__(self, other):
        """Scalar multiplication"""
        return Vector(x=self.x * other, y=self.y * other)

    def __truediv__(self, other):
        """Scalar division"""
        return Vector(x=self.x / other, y=self.y / other)
    
    def __floordiv__(self, other):
        """Scalar division"""
        return Vector(x=self.x // other, y=self.y // other)
    

    def __eq__(self, other):
        try:
            return self.x == other.x and self.y == other.y
        except AttributeError:
            return False

    def __getitem__(self, index):
        if index == 0:
            return self.x
        if index == 1:
            return self.y
        raise IndexError(f'Vectors only have two items, not {index}.')

    def __repr__(self):
        return f'Vector(x={self.x}, y={self.y})'

    def __str__(self):
        return f'({self.x}, {self.y})'
    
    def __round__(self):
        return Vector(x=round(self.x), y=round(self.y))
