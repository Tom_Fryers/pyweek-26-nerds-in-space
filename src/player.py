# Floodboat
# =========
#
# player.py

import pygame

import vectors
import funcs
import collisions
import controls

COLLISIONRANGE = 12

class Player:
    def __init__(self, imageloc, worldsize, xpos=None, moving=None, facing=0):
        if xpos is None:
            xpos = (worldsize + 1) * 32
        if moving is None:
            moving = vectors.Vector(x=0, y=3.6)
        self.position = vectors.Vector(x=xpos, y=6 * 64)
        self.facing = facing
        self.image = pygame.image.load(imageloc)
        self.prevrot = None
        self.previm = None
        self.rot = 0
        self.moving = moving
        self.drawimage = None
        self.worldsize = worldsize
        self.range = COLLISIONRANGE

    def move(self, inputs, world):
        if inputs['Left']:
            self.rot -= 0.1
        if inputs['Right']:
            self.rot += 0.1

        # Slow down rotation so they can't infinitely spin
        self.rot *= 0.95

        newspeed = 0
        if inputs['Forwards']:
            newspeed += 0.15
        if inputs['Backwards']:
            newspeed -= 0.12

        newspeed = vectors.Vector(magnitude=newspeed, direction=self.facing)
        self.moving += newspeed
        self.moving += world.current

        # Stop the player going too high
        if self.position[1] < 14 * 64:
            self.moving.y += min(0.0025 * (14 * 64 - self.position[1]), 0.15)

        # Push the player off if they're too low
        if self.position[1] > (self.worldsize - 13) * 64:
            self.moving.y += max(0.0025 * ((self.position[1] - self.worldsize) / 32), -0.005)

        # Apply drag
        self.moving *= 0.96

        # Do collisions
        colls = []
        tilein = (world.to_tile(self.position[0]),
                  world.to_tile(self.position[1]))

        # Iterate through all tiles in the vicinity
        for x in range(-1, 2):
            for y in range(-1, 2):
                collision = False
                tilex = (tilein[0] + x) * 64
                tiley = (tilein[1] + y) * 64

                # Do not collide if the tile is passable
                if world.map[x + tilein[0]][y + tilein[1]].ispassable:
                    continue

                ## assert y != 0 or x != 0

                # Do corner collisions
                for corner in ((0, 0), (0, 64), (64, 0), (64, 64)):
                    cornerloc = vectors.Vector(tilex + corner[0],
                                               tiley + corner[1])
                    # Define a vector pointing from the player to the corner
                    tocorner = self.position - cornerloc
                    if tocorner.magnitude < COLLISIONRANGE - 1:
                        collision = True
                        self.moving = self.moving.remove_direction(tocorner.direction)

                # Do edge collisions
                if collisions.circle_collides_axis(self.position, COLLISIONRANGE, ((tilex, tiley), (tilex, tiley + 64))):
                    self.moving.x = min(0, self.moving.x)
                    collision = True

                elif collisions.circle_collides_axis(self.position, COLLISIONRANGE, ((tilex, tiley), (tilex + 64, tiley))):
                    self.moving.y = min(0, self.moving.y)
                    collision = True

                elif collisions.circle_collides_axis(self.position, COLLISIONRANGE, ((tilex + 64, tiley), (tilex + 64, tiley + 64))):
                    self.moving.x = max(0, self.moving.x)
                    collision = True

                elif collisions.circle_collides_axis(self.position, COLLISIONRANGE, ((tilex, tiley + 64), (tilex + 64, tiley + 64))):
                    self.moving.y = max(0, self.moving.y)
                    collision = True

                if collision:
                    colls.append((tilein[0] + x, tilein[1] + y))
                self.moving.rec_to_pol()

        self.facing += self.rot
        self.position += self.moving

        if abs(self.rot) < 0.03:
            self.rot = 0
        if self.moving.magnitude < 0.03:
            self.moving = vectors.Vector(x=0, y=0)

        return colls

    def draw(self):
        if self.prevrot != self.facing:
            self.drawimage = funcs.rotate_proper(self.image, -self.facing)
        return self.drawimage
