# Floodboat
# =========
#
# stick.py

import random
import funcs
import vectors

class Stick:
    def __init__(self, image, position, size, imsize, worldsize):
        self.size = size
        self.imsize = imsize
        self.image = image
        self.position = position
        self.drawposition = self.position
        self.offset = vectors.Vector(imsize / 2, imsize / 2)
        self.find_tile()
        self.rotation = random.uniform(0, 360)
        self.rotspeed = random.uniform(-2, 2)
        self.moving = vectors.Vector(0, 0)
        self.worldsize = worldsize

    def draw(self):
        self.drawimage = funcs.rotate_proper(self.image, self.rotation)
        return self.drawimage

    def move(self, current):
        self.rotation += self.rotspeed

        # Use same fastwater logic
        if self.position[1] < 14 * 64:
            self.moving.y += min(0.0025 * (14 * 64 - self.position[1]), 0.15)

        elif self.position[1] > (self.worldsize - 13) * 64:
            self.moving.y += max(0.0025 * ((self.position[1] - self.worldsize) / 32), -0.005)
        else:
            self.moving += current
        
        # Apply drag
        self.moving *= 0.96
        self.position += self.moving
        self.position.rec_to_pol()
        self.find_tile()

    def find_tile(self):
        self.tilein = round(self.position) // 64

    def collide(self, themap):
        if not themap[self.tilein[0]][self.tilein[1]].ispassable:
            return self.tilein
