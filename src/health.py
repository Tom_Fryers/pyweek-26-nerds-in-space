# Floodboat
# =========
# 
# health.py

class HealthObject:
    """A class containing functions related to objects with health"""
    def damage(self, kill=False, damage=None):
        """Damages, or instantly kills the object. Returns True on death."""
        if not kill and damage is not None and damage > 0:
            self.health -= damage
            if self.health <= 0:
                self.health = 0
                kill = True
        if kill:
            self.health = 0
            return True

    def heal(self, fullheal=False, setvalue=None, heal=None):
        """Full heals, Sets health to a value, or heals for a value"""
        if fullheal == True and setvalue == None and heal == None:
            self.health = self.maxhealth
        elif fullheal == False and setvalue != None and heal == None and setvalue > 0 and setvalue <= self.maxhealth:
            self.health = setvalue
        elif fullheal == False and setvalue == None and heal != None and heal > 0:
            self.health += heal
            if self.health > self.maxhealth:
                self.health = self.maxhealth
        else:
            raise ValueError(f'Invalid Variables. fullheal is bool, setvalue and heal are int. Only one of fullheal, setvalue and heal can be given. setvalue must be between 1 and {self.maxhealth}.')
