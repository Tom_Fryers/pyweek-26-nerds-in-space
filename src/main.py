# Floodboat
# =========
#
# main.py

# Import standard library modules
import sys
import random
import math
import time  # Currently debug only

# Import pygame modules
import pygame
from pygame.locals import *


if __name__ == '__main__':
    raise RuntimeError('Use run_game.py')


# Import other files
import controls
import player
import world
import funcs
import vectors
import healthbar
import stick
import collisions

# Pygame initialisation
pygame.mixer.pre_init(44100, -16, 1, 512)
pygame.init()

pygame.mixer.music.load('data/sound/Plans in Motion.ogg')

WIDTH = 800
HEIGHT = 600

S = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Floodboat')

FPS = 60  # FPS *limit*

TITLEFONT = pygame.font.Font('data/font/FiraSans-Heavy.otf', 80)
OPTIONSFONT = pygame.font.Font('data/font/FiraSans-Heavy.otf', 40)
SMALLMENUFONT = pygame.font.Font('data/font/FiraSans-Medium.otf', 18)
LEVELFONT = pygame.font.Font('data/font/FiraSans-Light.otf', 20)

STICKIMAGES = ['stick1.png',
               'stick2.png',
               'stick3.png',
               'stick4.png',
               ]
for i in range(len(STICKIMAGES)):
    STICKIMAGES[i] = pygame.image.load('data/images/' + STICKIMAGES[i]).convert_alpha()
ns = []
for i in STICKIMAGES:
    ns.append(pygame.transform.flip(i, 1, 0))

STICKIMAGES = tuple(STICKIMAGES + ns)
TARGET = pygame.image.load('data/images/target.png').convert_alpha()
ARROW = pygame.image.load('data/images/arrow.png')

clicksound = pygame.mixer.Sound('data/sound/click.wav')

clock = pygame.time.Clock()  # Needed for FPS cap

MAXHEALTH = 4
SSR = 100
STICKSIZE = 13

settings = open('src/settings.txt')
for line in settings:
    if 'Difficulty' in line:
        difficulty = line[line.find(' ') + 1:-1]
    else:
        raise NotImplementedError(f'Setting with no use case \'{line[:-1]}\'')


def targets_needed(level):
    """The number of targets that must be collected each level"""
    return round(level * 2 + 3)

def current_strength(level):
    """The strength of the downwards current"""
    return (level ** 0.3) / 50

def target_chance(level, targets):
    """The probablility (1/) that a new target is created each frame"""
    return round((500 - math.log(level) * 150) * (len(targets) ** 2.2))

def stick_chance(level, sticks):
    """The probablility (1/) a stick is generated each frame"""
    return round(((len(sticks) + 1) ** 0.5 / (level_size(level) - 26) ** 2) * 3000 / (level ** 0.8))

def level_size(level):
    """The size, in tiles, of the level"""
    return round(38 + (level * 1.5))


# Precedures
def main_menu(menuselected=0):
    """Displays the main menu"""
    
    water = pygame.transform.scale2x(pygame.transform.scale2x(pygame.transform.scale2x(pygame.transform.scale2x(pygame.image.load('data/images/watertex.png')))))
    background = pygame.Surface((WIDTH, HEIGHT))
    background.blit(water, (0, 0))
    background.blit(pygame.transform.rotate(pygame.image.load('data/images/boatlarge.png'), 90), ((WIDTH - 736) / 2, (HEIGHT - 736) / 2))
    selection = menu('FLOODBOAT', TITLEFONT, ['Play', 'Instructions', 'Settings', 'Quit'], OPTIONSFONT, background=background, selected=menuselected)

    if selection == 'Play':
        game()
    elif selection == 'Instructions':
        instructions_menu()
        main_menu(menuselected=1)
    elif selection == 'Settings':
        settings_menu()
        main_menu(menuselected=2)
    elif selection == 'Quit':
        pygame.quit()
        sys.exit()

def pause_menu(screen, menuselected=0):
    """Displays the pause menu"""
    background = pygame.Surface((WIDTH, HEIGHT))
    background.blit(screen, (0, 0))

    selection = menu('Pause', TITLEFONT, ['Resume', 'Instructions', 'Settings', 'Quit to Main Menu'], OPTIONSFONT, background=background, selected=menuselected)

    if selection == 'Resume':
        return False
    elif selection == 'Instructions':
        instructions_menu()
        pause_menu(screen, menuselected=1)
    elif selection == 'Settings':
        settings_menu()
        pause_menu(screen, menuselected=2)
    else:
        S.fill((0, 0, 0))
        main_menu()
        raise SystemExit

def settings_menu(menuselected=0):
    """Displays the settings menu, and allows for controls to be changed"""
    global difficulty

    S.fill((0, 0, 0))

    selection = menu('SETTINGS', TITLEFONT, ['Controls', 'Difficulty', 'Back'], OPTIONSFONT, selected=menuselected)

    if selection == 'Controls':
        controls_menu()

    elif selection == 'Difficulty':
        selected = 0
        while True:
            selection = menu('SETTINIGS', TITLEFONT, ['Easy', 'Normal', 'Hard', 'Back'], OPTIONSFONT, [f'Current Difficulty: {difficulty}'], SMALLMENUFONT, selected=selected)

            if selection == 'Back':
                settings_menu(menuselected=1)
                break

            else:
                selected = ['Easy', 'Normal', 'Hard'].index(selection)
                difficulty = selection
                with open('src/settings.txt', 'r') as settingsfile:
                    text = settingsfile.read().split('\n')
                    settings = []
                    for line in text:
                        settings.append(line)
                with open('src/settings.txt', 'w') as settingsfile:
                    for setting in settings:
                        if 'Difficulty' in setting:
                            settingsfile.write(f'Difficulty: {difficulty}\n')
                        else:
                            settingsfile.write(setting)

def controls_menu(menuselected=0):
    newmovecontrols = {}
    newothercontrols = {}
    for i in controls.MOVECONTROLS:
        newmovecontrols[controls.MOVECONTROLS[i]] = i
    for i in controls.OTHERCONTROLS:
        newothercontrols[controls.OTHERCONTROLS[i]] = i
    while True:
            S.fill((0, 0, 0))

            controlmenuoptions = [f'Forwards:     {pygame.key.name(newmovecontrols["Forwards"]).upper()}',
                                  f'Backwards:    {pygame.key.name(newmovecontrols["Backwards"]).upper()}',
                                  f'Left:         {pygame.key.name(newmovecontrols["Left"]).upper()}',
                                  f'Right:        {pygame.key.name(newmovecontrols["Right"]).upper()}',
                                  f'Pause:        {pygame.key.name(newothercontrols["Pause"]).upper()}',
                                  'Back']

            selection = menu('SETTINGS', TITLEFONT, controlmenuoptions, OPTIONSFONT, ['Press Enter to', 'change a control'], SMALLMENUFONT, selected=menuselected)

            if selection == 'Back':
                controls.MOVECONTROLS = {}
                controls.OTHERCONTROLS = {}
                for i in newmovecontrols:
                    controls.MOVECONTROLS[newmovecontrols[i]] = i
                for i in newothercontrols:
                    controls.OTHERCONTROLS[newothercontrols[i]] = i
                controls.write_controls()
                settings_menu(menuselected=0)
                break

            selection = selection[:selection.find(':')]

            instructiontext = SMALLMENUFONT.render(f'Press a new key to bind it to "{selection}"', 1, (255, 255, 255))
            S.blit(instructiontext,(funcs.get_centre_blit(instructiontext, WIDTH, side='x'), HEIGHT - 50))
            pygame.display.update()

            ext = False
            while not ext:
                for event in pygame.event.get():
                    if event.type == KEYDOWN:
                        newkey = event.key
                        ext = True
                        break
            if selection in newmovecontrols.keys():
                newmovecontrols[selection] = newkey
            else:
                newothercontrols[selection] = newkey

            menuselected = [x[:x.find(':')] for x in controlmenuoptions].index(selection)

def death_menu(reason, level, score, menuselected=0):
    """The menu displayed upon death"""
    selection = menu('GAME OVER', TITLEFONT, ['Restart', 'Save Score', 'Scoreboard', 'Quit to Main Menu'], OPTIONSFONT, [reason, f'You survived until level {level}', f'You Scored {str(score).replace("-", "−")}'], SMALLMENUFONT, selected=menuselected)

    if selection == 'Restart':
        game()

    elif selection == 'Save Score':
        write_score(level, score)
        death_menu(reason, level, score, menuselected=1)

    elif selection == 'Scoreboard':
        scoreboard()
        death_menu(reason, level, score, menuselected=2)

    elif selection == 'Quit to Main Menu':
        main_menu(menuselected=0)
        pygame.quit()
        sys.exit()

def win_menu(score, menuselected=0):
    """Displays the winscreen and the score"""
    menuselected = 0
    selection = menu("YOU WIN", TITLEFONT, ['Restart', 'Save Score', 'Scoreboard', 'Quit to Main Menu'], OPTIONSFONT, ['You got everyone to safety!', f'You Scored {str(round(score)).replace("-", "−")}'], SMALLMENUFONT, selected=menuselected)
    
    if selection == 'Restart':
        game()
    
    elif selection == 'Save Score':
        write_score()
        win_menu(menuselected=1)
        
    elif selection == 'Scoreboard':
        scoreboard()
        win_menu(menuselected=2)
    
    elif selection == 'Quit to Main Menu':
        main_menu(menuselected=0)
        pygame.quit()
        sys.exit()

def write_score(finallevel, score):
    """Writes the score from the current game to scoreboard.txt"""
    name = ''
    ext = False
    while not ext and len(name) <= 24:
        S.fill((0, 0, 0))
        S.blit(SMALLMENUFONT.render(f'Type Your Name: {name}', 1, (255, 255, 255)), (100, (HEIGHT - SMALLMENUFONT.get_height()) // 2))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key in list(range(97, 123)) + list(range(48, 58)) + [32]:
                    mods = pygame.key.get_mods()
                    if (mods and KMOD_LSHIFT) or (mods and KMOD_RSHIFT):
                        name += chr(event.key).upper()
                    else:
                        name += chr(event.key).lower()
                elif event.key == K_BACKSPACE:
                    print
                    name = name[:-1]
                elif event.key == K_RETURN:
                    ext = True
    name = name.replace(' ', '_')
    
    scoreboardfile = open('src/scoreboard.txt', 'a')
    scoreboardfile.write(f'{score} {name} {difficulty} {finallevel}\n')
    scoreboardfile.close()
    scoreboard()
    
def scoreboard():
    """Displays the Scoreboard"""
    
    with open('src/scoreboard.txt', 'r') as f:
        scores = [x for x in f.read().split('\n')]
    scores = scores[:-1]

    for i in range(len(scores)):
        scores[i] = scores[i].split(' ')
        scores[i][0] = int(scores[i][0])
    scores.sort(key=lambda x: x[0], reverse=True)
    
    if len(scores) > 10:
        scores = scores[:10]
    
    S.fill((0, 0, 0))
    
    titleimage = TITLEFONT.render('HIGHSCORES', 1, (255, 255, 255))
    
    S.blit(titleimage, (funcs.get_centre_blit(titleimage, WIDTH, side='x'), round(TITLEFONT.get_height() * 1 / 4)))
    S.blit(OPTIONSFONT.render('Name', 1, (255, 255, 255)), (40, round(TITLEFONT.get_height() * 5 / 4)))
    S.blit(OPTIONSFONT.render('Score', 1, (255, 255, 255)), (WIDTH / 2 - 175, round(TITLEFONT.get_height() * 5 / 4)))
    S.blit(OPTIONSFONT.render('Difficulty', 1, (255, 255, 255)), (WIDTH / 2 - 50, round(TITLEFONT.get_height() * 5 / 4)))
    S.blit(OPTIONSFONT.render('Level Reached', 1, (255, 255, 255)), (WIDTH / 2 + 125, round(TITLEFONT.get_height() * 5 / 4)))
    
    for i in range(len(scores)):
        S.blit(SMALLMENUFONT.render(f'{i}', 1, (255, 255, 255)), (10, round(TITLEFONT.get_height() * 5 / 4 + OPTIONSFONT.get_height() * 5 / 4 + SMALLMENUFONT.get_height() * i * 6 / 5)))
        S.blit(SMALLMENUFONT.render(f'{scores[i][1].replace("_", " ")}', 1, (255, 255, 255)), (40, round(TITLEFONT.get_height() * 5 / 4 + OPTIONSFONT.get_height() * 5 / 4 + SMALLMENUFONT.get_height() * i * 6 / 5)))
        S.blit(SMALLMENUFONT.render(f'{scores[i][0] + 1}', 1, (255, 255, 255)), (WIDTH / 2 - 175, round(TITLEFONT.get_height() * 5 / 4 + OPTIONSFONT.get_height() * 5 / 4 + SMALLMENUFONT.get_height() * i * 6 / 5)))
        S.blit(SMALLMENUFONT.render(f'{scores[i][2]}', 1, (255, 255, 255)), (WIDTH / 2 - 50, round(TITLEFONT.get_height() * 5 / 4 + OPTIONSFONT.get_height() * 5 / 4 + SMALLMENUFONT.get_height() * i * 6 / 5)))
        S.blit(SMALLMENUFONT.render(f'{scores[i][3]}', 1, (255, 255, 255)), (WIDTH / 2 + 125, round(TITLEFONT.get_height() * 5 / 4 + OPTIONSFONT.get_height() * 5 / 4 + SMALLMENUFONT.get_height() * i * 6 / 5)))
    
    S.blit(OPTIONSFONT.render('Back', 1, (100, 255, 255)), (100, HEIGHT - 100 - OPTIONSFONT.get_height()))
    pygame.display.update()
    ext = False
    while not ext:
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key in [K_RETURN, K_SPACE, K_RIGHT, K_d]:
                    ext = True

def instructions_menu():
    """Displays the game instructions"""
    
    with open('src/instructions.txt', 'r') as f:
        instructions = f.read().split('\n')
    menu('FLOODBOAT', TITLEFONT,
         ['Back'], OPTIONSFONT, instructions, SMALLMENUFONT)

def game():
    """Runs the main script of the game"""
    if not pygame.mixer.music.get_busy():
        pygame.mixer.music.play(0)
    xpos = None
    newmoving = None
    oldfacing = 0
    gameexit = False
    dead = False
    playerhealth = healthbar.Healthbar(MAXHEALTH)
    playerinput = {x: None for x in controls.MOVECONTROLS.values()}
    score = 0
    lastscore = None
    deadcos = None
    oldback = None
    lastpeople = None
    for level in range(1, 10):
        truelevel = level
        levelimage = LEVELFONT.render(f'Level {level}, {difficulty}', 1, (0, 0, 0))
        level *= {'Easy': 1/1.5, 'Normal': 1, 'Hard': 1.4}[difficulty]
        frames = 0
        # Initialise
        size = level_size(level)
        people = targets_needed(level)
        got = 0

        if xpos is not None and xpos < 0:
            xchange = (size * 64 + xpos) - (lastsize * 64 + xpos)
            xpos = size * 64 + xpos
        else:
            xchange = 0

        try:
            sticks = filter(lambda s: (xpos - xchange - WIDTH / 2 - SSR < s.position.x < xpos - xchange + WIDTH / 2 + SSR) and (ypos - HEIGHT / 2 - SSR < s.position.y < ypos + HEIGHT / 2 + SSR), sticks)
        except NameError:
            sticks = []
            for i in range(round(level * 2)):
                sticks.append(stick.Stick(random.choice(STICKIMAGES), vectors.Vector(x=random.uniform(13 * 64, (size - 13) * 64), y=random.uniform(6 * 64, 15 * 64)), STICKSIZE, 32, size))

        theplayer = player.Player('data/images/boat.png', worldsize=size, xpos=xpos, moving=newmoving, facing=oldfacing)
        theworld = world.World(size, vectors.Vector(x=0, y=current_strength(level)))
        if oldback is not None:
            theworld.backdrawposf = oldback
        targets = []
        if isinstance(sticks, filter):
            sticks = list(sticks)
            for astick in sticks:
                astick.worldsize = size
                astick.position.y = astick.position.y - ypos + theplayer.position.y
                astick.position.x += xchange
            while len(sticks) < level * 2:
                s = stick.Stick(random.choice(STICKIMAGES), vectors.Vector(x=random.uniform(13 * 64, (size - 13) * 64), y=random.uniform(6 * 64, 15 * 64)), STICKSIZE, 32, size)
                if not (theplayer.position.x - WIDTH / 2 - SSR < s.position.x < theplayer.position.x + WIDTH / 2 + SSR) and (theplayer.position.y - HEIGHT / 2 - SSR < s.position.y < theplayer.position.y + HEIGHT / 2 + SSR):
                    sticks.append(s)

        # Game loop
        ext = False
        while not ext:
            # Event handling loop
            for event in pygame.event.get():
                if event.type == QUIT:
                    gameexit = True
                    ext = True
                    break

                elif event.type == KEYDOWN:
                    try:
                        playerinput[controls.MOVECONTROLS[event.key]] = True
                    except KeyError:
                        try:
                            if controls.OTHERCONTROLS[event.key] == 'Pause':
                                for i in playerinput:
                                    playerinput[i] = False
                                gameexit = pause_menu(S.copy())
                                ext = gameexit
                            else:
                                raise NotImplementedError(f'{event.key} in controls.OTHERCONTROLS with no use case.')
                        except KeyError:
                            print(f'The key {event.key} does nothing.')

                elif event.type == KEYUP:
                    try:
                        playerinput[controls.MOVECONTROLS[event.key]] = False
                    except KeyError:
                        pass

            if ext:
                break

            # Create new targets
            if got + len(targets) < people:
                if not random.randint(0, target_chance(level, targets)):
                    t = theworld.create_target(level)
                    if t is None:
                        dead = True
                        deadcos = 'All the houses on the level were destroyed.'
                        break
                    targets.append(t)

            # Create new sticks
            if not random.randint(0, stick_chance(level, sticks)):
                pos = vectors.Vector(x=random.randint(13 * 64, (size - 13) * 64), y=64)
                sticks.append(stick.Stick(random.choice(STICKIMAGES), pos, STICKSIZE, 32, size))

            # Test for stick collisions
            damages = []
            newsticks = []
            for astick in sticks:
                astick.move(theworld.current)
                # Determine if it hits the player
                if collisions.circle_collides_circle(astick.position, theplayer.position, astick.size + theplayer.range):
                    if frames < 100:
                        continue
                    playerhealth.damage(kill=False, damage=1)
                    score -= 100
                    if not playerhealth.health:
                        deadcos = 'You were hit by debris.'
                    continue
                # Determine if it hits a house
                col = astick.collide(theworld.map)
                if col:
                    if theworld.damage(col):
                        score -= 5
                    damages.append(col)
                # Destroy it if it goes off the map
                elif astick.position.y > (size - 1) * 64:
                    pass
                # Keep it
                else:
                    newsticks.append(astick)
            sticks = newsticks

            # Move the player, and get house collisions
            collision = theplayer.move(playerinput, theworld)
            
            tupledamages = [x.to_tuple() for x in damages]
            oldtargets = targets.copy()
            for target in oldtargets:
                # Determine if the player tagged a target, and remove it
                if target.tile in collision:
                    targets.remove(target)
                    got += 1
                    score += level * 100
                # Damage targets
                elif target.tile in tupledamages:
                    dead = target.damage()
                    if dead:
                        playerhealth.damage(kill=False, damage=1)
                        if not playerhealth.health:
                            deadcos = "A person's house was destroyed."
                            dead = True
                        targets.remove(target)
                        score -= 200

            # Send player to next level
            if theplayer.position[1] > (size - 6) * 64:
                # Punish naughty players
                if len(targets):
                    missed = people - got
                    score -= missed * 1000
                    playerhealth.damage(kill=False, damage=missed)
                    if not playerhealth.health:
                        deadcos = 'You abandoned everyone.'
                        dead = True

                else:
                    score += level * 30000 / frames ** 0.5
                oldfacing = theplayer.facing
                ypos = theplayer.position.y
                lastsize = size
                if theplayer.position[0] < (size * 64 / 2):
                    xpos = theplayer.position.x
                else:
                    xpos = theplayer.position.x - (size * 64)
                newmoving = theplayer.moving
                oldback = theworld.backdrawposf
                break

            # Determine if the player is dead
            if not playerhealth.health:
                dead = True
                break

            frames += 1
            ##playerdrawtime = time.time()
            # Draw the world
            S.blit(theworld.draw(theplayer.position, WIDTH, HEIGHT), (0, 0))
            ##print(f'World draw: {(time.time() - playerdrawtime) * 1000}')
            # Draw the healthbar
            S.blit(playerhealth.draw(), (12, 12))

            # Draw the level text
            S.blit(levelimage, (12, 45))

            # Draw the score
            if score != lastscore:
                scoreim = LEVELFONT.render(f'Score: {str(round(score)).replace("-", "−")}', 1, (0, 0, 0))
            S.blit(scoreim, (12, 70))
            lastscore = score
            
            # Draw the people-counter
            if people - got != lastpeople:
                peopleim = LEVELFONT.render(f'{people - got} people remaining', 1, (0, 0, 0))

            S.blit(peopleim, (12, 95))

            # Draw the sticks
            for astick in sticks:
                pos = (astick.position - astick.offset - theplayer.position + vectors.Vector(x=WIDTH / 2, y=HEIGHT / 2))
                if -32 < pos.x < WIDTH and -32 < pos.y < HEIGHT:
                    S.blit(astick.draw(), pos.to_tuple())

            # Draw the targets
            for target in targets:
                # Targets themselves
                drawloc = target.position - (theplayer.position - vectors.Vector(x=WIDTH / 2, y=HEIGHT / 2)) - vectors.Vector(16, 16)
                if -32 < drawloc.x < WIDTH and -32 < drawloc.y < HEIGHT:
                    S.blit(TARGET, drawloc.to_tuple())

                # Target Dots
                tovector = target.position - theplayer.position
                circlepos = vectors.Vector(magnitude=30, direction=tovector.direction) + vectors.Vector(x=WIDTH / 2, y=HEIGHT / 2)

                pygame.draw.circle(S, target.colour, round(circlepos).to_tuple(), 4)

            if not people - got:
                S.blit(ARROW, (WIDTH / 2 - 8, HEIGHT / 2 - 8 + 30))
                
            # Draw the player
            S.blit(theplayer.draw(), (WIDTH // 2 - 23, HEIGHT // 2 - 23))
            pygame.display.update()

            # Stop the game running too fast
            clock.tick(FPS)
            try:
                1/0
                print(f'Frame total: {clock.get_rawtime()} / {clock.get_time()}')
            except ZeroDivisionError:
                pass

        if gameexit:
            break
        if dead:
            dead = False
            death_menu(deadcos, truelevel, round(score))

# Functions
def menu(title, titlefont, optionstext, optionsfont, smalltext=None, smalltextfont=None, background=None, selected=0):
    """General menu function which returns a string of the option chosen

       Arguments:
       title – String, main unselectable text to be displayed
       titlefont – Pygame Font Object, the font to be used when displaying title
       optionstext – List of Strings, the selectable options to be displayed on the menu
       optionsfont – Pygame Font Object, the font to be used when displaying the options
       smalltext - List of Strings, unselectable smaller text e.g. subtitle, or extra information, each item in the list is a new line
       smalltextfont - Pygame Font Object, the font to be used when displaying smalltext
       background - Pygame Surface, the image to be displayed behind the text in the menu
       selected - Integer, the index of the option that will be highlighted when menu is called
    """
    
    if background is None:
        S.fill((0, 0, 0))
    else:
        S.blit(background, (0, 0))

    title = titlefont.render(title, 1, (255, 255, 255))

    options = []
    optionsselected = []
    optionsmaxwidth = 0
    for i in optionstext:
        options.append(optionsfont.render(i, 1, (255, 255, 255)))
        optionsselected.append(optionsfont.render(i, 1, (100, 255, 255)))
        if options[-1].get_width() > optionsmaxwidth:
            optionsmaxwidth = options[-1].get_width()
    optionsheight = optionsfont.get_height() * len(options) + (len(options) - 1) * optionsfont.get_height() // 5
    
    if smalltext != None:
        smalltextimages = []
        smalltextmaxwidth = 0
        for i in smalltext:
            smalltextimages.append(smalltextfont.render(i, 1, (255, 255, 255)))
            if smalltextimages[-1].get_width() > smalltextmaxwidth:
                smalltextmaxwidth = smalltextimages[-1].get_width()
        smalltextheight = smalltextfont.get_height() * len(smalltext) + (len(smalltext) - 1) * smalltextfont.get_height() // 5
        
        titlex = funcs.get_centre_blit(title, WIDTH, side='x')
        optionsx = (WIDTH - optionsmaxwidth - 100 - smalltextmaxwidth) / 2
        smalltextx = (WIDTH + optionsmaxwidth + 100 - smalltextmaxwidth) / 2
        
        if optionsheight >= smalltextheight:
            titley = (HEIGHT - 5 / 4 * titlefont.get_height() - optionsheight) / 2
            optionsy = (HEIGHT + 5 / 4 * titlefont.get_height() - optionsheight) / 2
            smalltexty = (HEIGHT + 5 / 4 * titlefont.get_height() - optionsheight) / 2 + (optionsheight - smalltextheight) / 2
        else:
            titley = (HEIGHT - 5 / 4 * titlefont.get_height() - smalltextheight) / 2
            optionsy = (HEIGHT + 5 / 4 * titlefont.get_height() - smalltextheight) / 2 + (smalltextheight - optionsheight) / 2
            smalltexty = (HEIGHT + 5 / 4 * titlefont.get_height() - smalltextheight) / 2
        
        for i in range(len(smalltextimages)):
            S.blit(smalltextimages[i], (smalltextx, smalltexty + i * (smalltextfont.get_height() + smalltextfont.get_height() // 5)))
    
    else:
        titlex = funcs.get_centre_blit(title, WIDTH, side='x')
        optionsx = 100
        
        titley = (HEIGHT - 5 / 4 * titlefont.get_height() - optionsheight) / 2
        optionsy = (HEIGHT + 5 / 4 * titlefont.get_height() - optionsheight) / 2

    S.blit(title, (titlex, titley))

    ext = False
    menuselected = selected
    while not ext:
        if background is None:
            S.fill((0, 0, 0))
        else:
            S.blit(background, (0, 0))
        S.blit(title, (titlex, titley))
        for i in range(0, len(optionstext)):
            if i != menuselected:
                S.blit(options[i], (optionsx, optionsy + i * (optionsfont.get_height() + optionsfont.get_height() // 5)))
            else:
                S.blit(optionsselected[i], (optionsx, optionsy + i * (optionsfont.get_height() + optionsfont.get_height() // 5)))
        if smalltext is not None:
            for i in range(len(smalltextimages)):
                print('Drawing')
                S.blit(smalltextimages[i], (smalltextx, smalltexty + i * (smalltextfont.get_height() + smalltextfont.get_height() // 5)))
        

        pygame.display.update()
        for event in pygame.event.get():
            if event.type == QUIT:
                ext = True
                break

            elif event.type == KEYDOWN:
                if event.key in (K_DOWN, K_s) and menuselected != len(optionstext) - 1:
                    clicksound.play()
                    menuselected += 1
                elif event.key in (K_UP, K_w) and menuselected != 0:
                    clicksound.play()
                    menuselected -= 1

                elif event.key in (K_SPACE, K_RIGHT, K_RETURN, K_d) and menuselected != -1:
                    clicksound.play()
                    return optionstext[menuselected]


# Script

main_menu()

pygame.quit()
sys.exit()
