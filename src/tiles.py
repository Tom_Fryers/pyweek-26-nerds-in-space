# Floodboat
# =========
#
# tiles.py

import tile
TILES = [tile.Tile('Water', 'water', True),
         tile.Tile('Grass', 'grass', False),
         tile.Tile('Fast Water', 'fast', True),
         tile.Tile('Tree', 'tree', False, base='grass'),
         tile.Tile('Rock', 'rock', False, base='grass'),
         ]
