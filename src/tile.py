# Floodboat
# =========
#
# tile.py

import copy

import pygame
import random

pygame.init()

class Tile:
    def __init__(self, name, image, ispassable, size=64, base=None):
        self.name = name
        self.size = size
        if isinstance(image, pygame.Surface):
            self.image = image
            self.topobject = None
        else:
            if base is None:
                self.image = pygame.image.load(f'data/images/{image}.png')
                self.topobject = None
            else:
                self.image = pygame.image.load(f'data/images/{base}.png')
                self.topobject = pygame.image.load(f'data/images/{image}.png')

        self.ispassable = ispassable

    def __str__(self):
        return f'Tile {self.name}'

    def copy(self):
        if self.topobject is not None:
            image = pygame.Surface((self.size, self.size))
            image.blit(self.image, (0, 0))
            topobject = pygame.transform.flip(pygame.transform.rotate(self.topobject, 90 * random.randint(0, 3)), random.randint(0, 1), random.randint(0, 1))
            image.blit(topobject, (0, 0))
        return Tile(self.name, image, self.ispassable, size=self.size)
