# Floodboat
# =========
#
# run_game.py

import sys
if sys.version_info < (3, 6):
    raise RuntimeError('Requires Python 3.6+ (use of f-strings)')
    # I think this is the only 3.6+ feature we use, so it may work if you replace it
sys.path.append('src/')
import main
