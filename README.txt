Pyweek 26
=========

The code and artwork were created by us, QuantumPotato and LeopardShark (NerdsInSpaaaace), with a bias towards art and code, respectively. The font is Fira Sans (https://github.com/bBoxType/FiraSans) The music and sound are credited at the bottom of this README. To run the game, simply run `python run_game.py`. The menu is operated with the arrow keys, and the default (configurable) keybindings are WASD to move, and escape to pause.

You can change the difficulty in the settings menu.

You must have:
   Python 3.6
   Pygame 1.8+

The game is licensed under the GPLv3, see LICENSE.

Written for Pyweek 26, a fun competition in which small teams of participants compete to create games in one week, using Python.

Click sound in menus from freesound.org:
https://freesound.org/people/lebaston100/sounds/192271/

"Plans in Motion" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/ 
