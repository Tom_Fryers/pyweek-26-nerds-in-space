# Floodboat
# =========
#
# healthbar.py

import pygame

import health

class Healthbar(health.HealthObject):
    """The player's healthbar"""
    def __init__(self, maxhealth):
        self.heartfull = pygame.image.load('data/images/heartfull.png')
        self.heartempty = pygame.image.load('data/images/heartempty.png')
        self.maxhealth = maxhealth
        self.health = maxhealth

    def draw(self):
        displayed = pygame.Surface((self.maxhealth * 30 - 6, 24), pygame.SRCALPHA, 32)
        displayed.convert_alpha()
        for i in range(self.maxhealth):
            if i < self.health:
                displayed.blit(self.heartfull, (30 * i, 0))
            else:
                displayed.blit(self.heartempty, (30 * i, 0))
        return displayed
    
    def __repr__(self):
        return f'Healthbar({self.maxhealth})'
    
    def __repr__(self):
        return f'Healthbar({self.health} / {self.maxhealth})'
    
