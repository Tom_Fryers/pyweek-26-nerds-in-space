# Floodboat
# =========
#
# house.py

import random

import pygame

import tile
import health

class House(tile.Tile, health.HealthObject):
    """A class that creates variable graphics for houses, and also includes their health"""
    def __init__(self, name, size=64):
        self.name = name
        self.size = size
        self.ispassable = False
        self.health = 4
        self.house = pygame.transform.rotate(pygame.image.load(f'data/images/house{random.randint(1, 3)}.png'), 90 * random.randint(0, 3))
        self.damages = []
        damagegraphicrotation = 90 * random.randint(0, 3)
        for i in range(1, 4):
            self.damages.append(pygame.transform.rotate(pygame.image.load(f'data/images/damage{i}.png'), damagegraphicrotation))
        self.draw()

    def draw(self):
        displayed = self.house
        if self.health != 4:
            displayed = self.damages[3 - self.health]
        self.image = displayed
