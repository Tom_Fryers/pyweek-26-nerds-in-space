# Floodboat
# =========
#
# controls.py

from pygame.locals import *

controlscheme = open('src/controlscheme.txt')

MOVECONTROLS = {}
OTHERCONTROLS = {}

line = controlscheme.readline()
while line != '\n':
    MOVECONTROLS[int(line[:line.find(' ')])] = line[line.find(' ') + 1:len(line) - 1]
    line = controlscheme.readline()
line = controlscheme.readline()
while line != '':
    OTHERCONTROLS[int(line[:line.find(' ')])] = line[line.find(' ') + 1:len(line) - 1]
    line = controlscheme.readline()
    
controlscheme.close()

def write_controls():
    controlscheme = open('src/controlscheme.txt', 'w')
    for i in MOVECONTROLS.keys():
        controlscheme.write(f'{i} {MOVECONTROLS[i]}\n')
    controlscheme.write('\n')
    for i in OTHERCONTROLS.keys():
        controlscheme.write(f'{i} {OTHERCONTROLS[i]}\n')
