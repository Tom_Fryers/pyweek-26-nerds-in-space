# Floodboat
# =========
#
# collisions.py

def point_in_rect(point, rect):
    """Determine if a point is inside a rect"""
    return rect[0] < point[0] < rect[0] + rect[2] and rect[1] < point[1] < rect[1] + rect[3]

def point_in_circle(point, circlepos, radius):
    """Determine if a point is inside a circle"""
    return (point[0] - circlepos[0]) ** 2 + (point[1] - circlepos[1]) ** 2 < radius ** 2

def circle_collides_circle(circle1pos, circle2pos, radialsum):
    """Deterimine if two circles collide"""
    return (circle1pos[0] - circle2pos[0]) ** 2 + (circle1pos[1] - circle2pos[1]) ** 2 < radialsum ** 2

def circle_collides_axis(circlepos, radius, line):
    """Determine if a circle collides with an orthogonal line"""
    if line[0][0] == line[1][0]:
        axis = 0
    elif line[0][1] == line[1][1]:
        axis = 1
    else:
        raise ValueError('Line is not orthogonal.')

    line = sorted(line, key=lambda x: x[1 - axis])
    withinrange = line[0][1 - axis] < circlepos[1 - axis] < line[1][1 - axis]
    if withinrange and False:
        print(line)
        print(circlepos)
    if withinrange and circlepos[axis] - radius < line[0][axis] < circlepos[axis] + radius:
        return True

    return False
