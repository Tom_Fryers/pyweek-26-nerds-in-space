# Floodboat
# =========
# 
# funcs.py

import pygame

def get_centre_blit(image, SIZE, side='x'):
    """Find the position to blit an image for it to be centred"""
    if side == 'x':
        return (SIZE - image.get_width()) / 2
    elif side == 'y':
        return (SIZE - image.get_height()) / 2

def crop(surface, size):
    """Crop a surface, centred, to a size"""
    # Create a new, transparent surface
    newsurface = pygame.Surface(size, pygame.SRCALPHA, 32)
    newsurface.convert_alpha()

    newsurface.blit(surface, (get_centre_blit(surface, size[0], side='y'), get_centre_blit(surface, size[1], side='y')))
    return newsurface

def rotate_proper(surface, angle):
    """Rotate a surface, preserving size"""
    size = (surface.get_width(), surface.get_height())
    newsize = [z * 2 for z in size]
    surface = pygame.transform.scale2x(surface)
    surface = pygame.transform.rotate(surface, angle)
    surface = crop(surface, newsize)
    surface = pygame.transform.smoothscale(surface, size)
    return surface
    
