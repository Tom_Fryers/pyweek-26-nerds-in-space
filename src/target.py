# Floodboat
# =========
#
# vectors.py

import vectors

TARGETCOLS = [(  0,   0,   0),
              (200,   0,   0),
              (255,  82,   0),
              (255, 255,   0),
              (  0, 200,  50)]

class Target:
    def __init__(self, position, hp):
        self.position = position
        self.tile = ((self.position - vectors.Vector(x=32, y=32)) // 64).to_tuple()
        self.hp = hp
        self.colour = TARGETCOLS[hp]
        self.dead = False

    def damage(self):
        self.hp -= 1
        self.colour = TARGETCOLS[self.hp]
        if self.hp == 0:
            self.dead = True

        return self.dead
