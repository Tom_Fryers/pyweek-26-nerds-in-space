# Floodboat
# =========
#
# world.py

import random

import pygame

import tiles
import target
import vectors
import house
import funcs

pygame.init()

DEBUGFONT = pygame.font.Font(None, 15)

def create_world(size):
    """Create a random world with given dimensions"""
    world = []
    for x in range(size):
        row = []
        for y in range(size):
            if not (13 < x < size - 13):
                if 13 < y < size - 13:
                    grasstype = random.choice([1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 4])
                else:
                    grasstype = 1

                if grasstype == 1:
                    t = tiles.TILES[grasstype]
                else:
                    t = tiles.TILES[grasstype].copy()
                row.append(t)
            elif not 13 < y < size - 13:
                row.append(tiles.TILES[2])
            elif random.randint(0, 2) and (14 < y < size - 14):
                row.append(house.House('House'))
            else:
                row.append(tiles.TILES[0])
        world.append(row)

    for i in range(round((size - 26) ** 2 * 2.3)):
        x = random.randint(14, size - 14)
        y = random.randint(14, size - 14)
        surrounds = sum(int('House' in world[x + z[0]][y + z[1]].name) for z in ((-1, 0), (1, 0), (0, -1), (0, 1))) +\
                    sum(int('House' in world[x + z[0]][y + z[1]].name) for z in ((-1, -1), (1, 1), (1, -1), (-1, 1))) / 3

        if random.randint(0, round(surrounds * 3)) > 2:
            world[x][y] = tiles.TILES[0]
    return world

class World:
    def __init__(self, size, current):
        self.map = create_world(size)
        self.size = size
        self.lastposition = None
        self.current = current
        self.strengthchange = 0
        self.directionchange = 0
        backimage = pygame.image.load('data/images/watertex.png')
        self.background = pygame.Surface((64 * 20, 64 * 15))
        for x in range(0, 20, 1):
            for y in range(0, 20, 1):
                self.background.blit(backimage, (x * 64, y * 64))
        self.backdrawpos = 0
        self.bpb = 0

        # Do the fast one
        self.backgroundf = pygame.Surface((64 * 20, 64 * 13))
        for x in range(0, 20, 1):
            for y in range(0, 20, 1):
                self.backgroundf.blit(backimage, (x * 64, y * 64))
        self.backdrawposf = 0
        self.bpbf = 3.6
        self._create_image()

    def _create_image(self):
        """Create the large map image"""
        self.image = pygame.Surface((self.size * 64, self.size * 64))
        self.image.fill((255, 0, 255))
        self.image.set_colorkey((255, 0, 255))
        for x in range(self.size):
            for y in range(self.size):
                ##debugim = DEBUGFONT.render(f'({x}, {y})', 1, (255, 255, 255))
                self.image.blit(self.map[x][y].image, (x * 64, y * 64))
                ##self.image.blit(debugim, (x * 64 + 10, y * 64 + 10))

    def draw(self, position, width, height):
        """Draw the world, given the player's position"""
        self.bpb += self.current.y
        self.bpb *= 0.97
        self.backdrawpos += self.bpb
        self.backdrawpos %= 64

        self.backdrawposf += self.bpbf
        self.backdrawposf %= 64

        drawpos = (-position.x % 64 - 64, (-position.y + self.backdrawpos) % 64 - 64)

        dps = False
        try:
            drawposf = (-position.x % 64 - 64, -position.y + height // 2 + self.backdrawposf)
            drawpossurf = pygame.Surface((width, 64 * 13 - (position.y - height / 2)))
            drawpossurf.blit(self.backgroundf, drawposf)
            dps = 'top'
        except pygame.error:
            try:
                drawposf = (-position.x % 64 - 64, (self.backdrawposf) % 64 - 64)
                drawpossurf = pygame.Surface((width, 13 * 64))
                drawpossurf.blit(self.backgroundf, drawposf)
                dps = 'bottom'
            except pygame.error:
                pass

        displayed = pygame.Surface((width, height))

        displayed.blit(self.background, drawpos)
        if dps == 'top':
            displayed.blit(drawpossurf, (0, 0))
        elif dps == 'bottom':
            displayed.blit(drawpossurf, (0, (self.size - 13) * 64 - position.y + height / 2))
        displayed.blit(self.image, (-position.x + width // 2, -position.y + height // 2))
        return displayed

    @staticmethod
    def to_tile(coord):
        """Find the tile a world coordinate is in"""
        return int(coord // 64)

    def create_target(self, level):
        """Create a target from the level"""
        houses = []
        for x in range(13, self.size - 13):
            for y in range(13, self.size - 13):
                t = self.map[x][y]
                if 'House' in t.name:
                    houses += [(x, y)] * max(1, round(t.health ** (10 - level) / 10))

        try:
            house = random.choice(houses)
        except IndexError:
            return None
        housepos = vectors.Vector(x=house[0], y=house[1]) * 64 + vectors.Vector(x=32, y=32)
        thetarget = target.Target(housepos, 4)
        for i in range(3 - self.map[house[0]][house[1]].health):
            thetarget.damage()
        return thetarget

    def damage(self, tile):
        """Damage a tile in the world"""
        res = False
        try:
            if self.map[tile[0]][tile[1]].damage(damage=1):
                self.map[tile[0]][tile[1]] = tiles.TILES[0]
                res = True
            else:
                self.map[tile[0]][tile[1]].draw()
            self.image.blit(self.map[tile[0]][tile[1]].image, (tile * 64).to_tuple())
        except AttributeError:
            pass
        return res
